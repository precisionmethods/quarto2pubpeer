Exposing some ideological subtext in Fourie Zirkelbach et al (2022)
================
David C. Norris
2/20/23

``` r
library(data.table)
library(jpeg)

## The ... argument lets us pass e.g. 'axes = TRUE' for the initial registration plot,
## and then 'axes = FALSE' upon subsequent use, when they are superfluous and take up
## too much space.
underlying <- function(image, xlim, ylim, ...) {
  
  plot(0:1, xlim = xlim, ylim = ylim
     , type='n', main=""
     , ...)
  
  rasterImage(png::readPNG(image)
             ,xleft = xlim[1]
             ,ybottom = ylim[1]
             ,xright = xlim[2]
             ,ytop = ylim[2]
              )
}
```

## Introduction

Although Fourie Zirkelbach et al. (2022) promise to “discuss the
underlying statistical principles” of “strategies to integrate dose
optimization into premarketing drug development”, this paper contains no
overt discussion, nor even a clear articulation, of any statistical
principle. Rather, it exhibits statistical
[anti-patterns](https://en.wikipedia.org/wiki/Anti-pattern) that
prefigure the ‘dose optimization’ methods advocated by FDA’s Oncology
Center of Excellence in recently issued Draft Guidance (OCE 2023).

## Fig 1B

Below I reproduce the article’s Fig 1B, with an overlay registering my
best-effort digitization.

``` r
## Read in digitized Fig 1B data, and overlay it on
## a png image, to prove registration is correct.

## TODO: Check that original study tallied 25 responses and 39 non,
##       for a total of N=64 participants.

fig1B <- function(...)
  underlying(image = "FZ-Fig1B.png"
           , xlim = c(-95, 538)
           , ylim = c(-.647, 1.304)
           , ...)

fig1B(axes = TRUE, xlab = "x", ylab = "y")
overlay <- adjustcolor("black", alpha.f = 0.35)
abline(v = c(0, 500), lty = 2, col= overlay)
abline(h = c(0, 1), lty = 2, col = overlay)

load("exposure_response.rda")
with(exposure_response, points(x, y, pch="+", col=overlay))

load("meancurve.rda")
with(meancurve, points(x, y, pch=20, col=overlay))

load("uclcurve.rda")
with(uclcurve, points(x, y, pch=20, col=overlay))

load("lclcurve.rda")
with(lclcurve, points(x, y, pch=20, col=overlay))
```

<figure>
<img src="FZ_files/figure-commonmark/fig-1B-1.png" id="fig-1B"
alt="Figure 1: Registration of digitized data from Fourie Zirkelbach et al. (2022), Fig 1B. The original caption reads, “Examples of exposure-efficacy relationships. … (B) Model-predicted versus observed relationship of pemigatinib exposure (Cmax, ss) and ORR. Red squares represent first (49.3–141 nM), second (143–185 nM), third (186–241 nM), and fourth (248–492 nM) quartiles of pemigatinib Cmax at steady state (Cmax, ss). Based on applicant figure reproduced in FDA Multidisciplinary review of pemigatinib.” Letting a probability confidence bound dip once into negative territory may be regarded as a misfortune, but allowing it to happen twice looks like carelessness." />
<figcaption aria-hidden="true">Figure 1: Registration of digitized data
from <span class="citation"
data-cites="fourie_zirkelbach_improving_2022">Fourie Zirkelbach et al.
(2022)</span>, Fig 1B. The original caption reads, “Examples of
exposure-efficacy relationships. … (B) Model-predicted versus observed
relationship of pemigatinib exposure (Cmax, ss) and ORR. Red squares
represent first (49.3–141 nM), second (143–185 nM), third (186–241 nM),
and fourth (248–492 nM) quartiles of pemigatinib Cmax at steady state
(Cmax, ss). Based on applicant figure reproduced in FDA
Multidisciplinary review of pemigatinib.” Letting a probability
confidence bound dip once into negative territory may be regarded as a
misfortune, but allowing it to happen twice looks like
carelessness.</figcaption>
</figure>

## Fitted Beta distributions

Where statistical principles are taken seriously, expressions of
uncertainty mean something. Attempting to take at least the predicted
mean and *upper* confidence bound in Fig 1B seriously, what do they
imply?

One way to evaluate the implied informational content of the confidence
band in Fig 1B is by fitting Beta distributions to the estimated
response probabilities. We use the method of moments (Gelman et al.
2014, Eq (A.3), p583).

``` r
xout <- seq(0, 400, 25)

df <- data.frame(x = xout
               , μ = with(meancurve, spline(x, y, xout=xout)$y)
               , ucl = with(uclcurve, spline(x, y, xout=xout)$y)
                 )

## Add method-of-moments estimates; see BDA3 Eq (A.3), p. 583.
df <- Hmisc::upData(df
                  , stderr = abs(ucl - μ)/qnorm(0.95)
                  , `α+β` = μ*(1-μ)/stderr^2 - 1
                  , α = `α+β`*μ
                  , β = `α+β` - α
                  , print = FALSE
                    )

knitr::kable(df, digits = 4)
```

<table>
<thead>
<tr class="header">
<th style="text-align: right;">x</th>
<th style="text-align: right;">μ</th>
<th style="text-align: right;">ucl</th>
<th style="text-align: right;">stderr</th>
<th style="text-align: right;">α+β</th>
<th style="text-align: right;">α</th>
<th style="text-align: right;">β</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0088</td>
<td style="text-align: right;">0.0336</td>
<td style="text-align: right;">0.0151</td>
<td style="text-align: right;">37.4802</td>
<td style="text-align: right;">0.3308</td>
<td style="text-align: right;">37.1494</td>
</tr>
<tr class="even">
<td style="text-align: right;">25</td>
<td style="text-align: right;">0.0204</td>
<td style="text-align: right;">0.0780</td>
<td style="text-align: right;">0.0350</td>
<td style="text-align: right;">15.2738</td>
<td style="text-align: right;">0.3115</td>
<td style="text-align: right;">14.9623</td>
</tr>
<tr class="odd">
<td style="text-align: right;">50</td>
<td style="text-align: right;">0.0493</td>
<td style="text-align: right;">0.1651</td>
<td style="text-align: right;">0.0704</td>
<td style="text-align: right;">8.4431</td>
<td style="text-align: right;">0.4160</td>
<td style="text-align: right;">8.0270</td>
</tr>
<tr class="even">
<td style="text-align: right;">75</td>
<td style="text-align: right;">0.1224</td>
<td style="text-align: right;">0.2772</td>
<td style="text-align: right;">0.0941</td>
<td style="text-align: right;">11.1325</td>
<td style="text-align: right;">1.3630</td>
<td style="text-align: right;">9.7696</td>
</tr>
<tr class="odd">
<td style="text-align: right;">100</td>
<td style="text-align: right;">0.2099</td>
<td style="text-align: right;">0.3714</td>
<td style="text-align: right;">0.0982</td>
<td style="text-align: right;">16.2103</td>
<td style="text-align: right;">3.4027</td>
<td style="text-align: right;">12.8076</td>
</tr>
<tr class="even">
<td style="text-align: right;">125</td>
<td style="text-align: right;">0.3052</td>
<td style="text-align: right;">0.4507</td>
<td style="text-align: right;">0.0885</td>
<td style="text-align: right;">26.0998</td>
<td style="text-align: right;">7.9654</td>
<td style="text-align: right;">18.1344</td>
</tr>
<tr class="odd">
<td style="text-align: right;">150</td>
<td style="text-align: right;">0.4066</td>
<td style="text-align: right;">0.5199</td>
<td style="text-align: right;">0.0689</td>
<td style="text-align: right;">49.8290</td>
<td style="text-align: right;">20.2603</td>
<td style="text-align: right;">29.5687</td>
</tr>
<tr class="even">
<td style="text-align: right;">175</td>
<td style="text-align: right;">0.4588</td>
<td style="text-align: right;">0.5865</td>
<td style="text-align: right;">0.0776</td>
<td style="text-align: right;">40.2055</td>
<td style="text-align: right;">18.4459</td>
<td style="text-align: right;">21.7596</td>
</tr>
<tr class="odd">
<td style="text-align: right;">200</td>
<td style="text-align: right;">0.4761</td>
<td style="text-align: right;">0.6190</td>
<td style="text-align: right;">0.0869</td>
<td style="text-align: right;">32.0648</td>
<td style="text-align: right;">15.2671</td>
<td style="text-align: right;">16.7978</td>
</tr>
<tr class="even">
<td style="text-align: right;">225</td>
<td style="text-align: right;">0.4543</td>
<td style="text-align: right;">0.5939</td>
<td style="text-align: right;">0.0848</td>
<td style="text-align: right;">33.4422</td>
<td style="text-align: right;">15.1935</td>
<td style="text-align: right;">18.2487</td>
</tr>
<tr class="odd">
<td style="text-align: right;">250</td>
<td style="text-align: right;">0.3868</td>
<td style="text-align: right;">0.5391</td>
<td style="text-align: right;">0.0926</td>
<td style="text-align: right;">26.6580</td>
<td style="text-align: right;">10.3107</td>
<td style="text-align: right;">16.3473</td>
</tr>
<tr class="even">
<td style="text-align: right;">275</td>
<td style="text-align: right;">0.2939</td>
<td style="text-align: right;">0.4727</td>
<td style="text-align: right;">0.1087</td>
<td style="text-align: right;">16.5680</td>
<td style="text-align: right;">4.8694</td>
<td style="text-align: right;">11.6986</td>
</tr>
<tr class="odd">
<td style="text-align: right;">300</td>
<td style="text-align: right;">0.1974</td>
<td style="text-align: right;">0.3887</td>
<td style="text-align: right;">0.1163</td>
<td style="text-align: right;">10.7113</td>
<td style="text-align: right;">2.1140</td>
<td style="text-align: right;">8.5973</td>
</tr>
<tr class="even">
<td style="text-align: right;">325</td>
<td style="text-align: right;">0.1097</td>
<td style="text-align: right;">0.2703</td>
<td style="text-align: right;">0.0976</td>
<td style="text-align: right;">9.2513</td>
<td style="text-align: right;">1.0152</td>
<td style="text-align: right;">8.2361</td>
</tr>
<tr class="odd">
<td style="text-align: right;">350</td>
<td style="text-align: right;">0.0488</td>
<td style="text-align: right;">0.1622</td>
<td style="text-align: right;">0.0690</td>
<td style="text-align: right;">8.7612</td>
<td style="text-align: right;">0.4276</td>
<td style="text-align: right;">8.3336</td>
</tr>
<tr class="even">
<td style="text-align: right;">375</td>
<td style="text-align: right;">0.0189</td>
<td style="text-align: right;">0.0789</td>
<td style="text-align: right;">0.0365</td>
<td style="text-align: right;">12.9046</td>
<td style="text-align: right;">0.2435</td>
<td style="text-align: right;">12.6611</td>
</tr>
<tr class="odd">
<td style="text-align: right;">400</td>
<td style="text-align: right;">0.0091</td>
<td style="text-align: right;">0.0327</td>
<td style="text-align: right;">0.0143</td>
<td style="text-align: right;">42.8959</td>
<td style="text-align: right;">0.3905</td>
<td style="text-align: right;">42.5054</td>
</tr>
</tbody>
</table>

On the view that the Beta(*α*,*β*) distribution is equivalent to having
observed *α* − 1 successes and *β* − 1 failures in *α* + *β* − 2
Bernoulli trials (Gelman et al. 2014, p35), the large implied *β*’s and
*α* \< 1 for *x* \> 350 suggest that the original Fig 1B embodies strong
priors far beyond the actual data collected — and indeed *beyond any
data that could possibly be collected,* since *α* \< 1 implies
observation of fewer than zero sucesses. Thus it becomes clear that,
beyond to the actual trial data collected, Fig 1B incorporates a large
contribution of statistically uncontrolled impressionistic content.

## On the ‘model’ used for ‘prediction’

The origins of Fig 1B are to be found in a figure from Incyte’s NDA
submission, reproduced as Figure 15 in (CDER 2018, p192):

<img src="MDRE-Fig15-plustext.png"
data-fig-alt="Figure 15 from an FDA Multidisciplinary Review and Evaluation of Incyte&#39;s NDA submission.  The accompanying text reads, &#39;The exposure-response relationship of ORR and pemigatinib exposure was also evaluated with participants in study INCB 54828-202 Cohort A. The relationship followed a bell-shaped curve and a binary logistic regression with quadratic function was used to evaluate the relationship. Cmax,ss was identified to have the strongest association with ORR based on AIC criteria. The model predicted ORR (42.8%) at dose 13.5 mg is similar as the ORR (44%) predicted from change in serum phosphate concentration from baseline. Model predicted vs. observed relationship of ORR and Cmax,ss was shown in Figure 15.&#39;" />

Thus, the “model-predicted ORR” proves to be anything but. Its ‘model’
is nothing more than an arbitrarily selected functional form lacking
substantive pharmacologic rationale. Nor is there anything ‘pre’ about
this decidedly *post*dictive curve-fitting exercise. We can throw this
arbitrariness into sharper relief by attempting our own reproductions of
the figure.

### Reproducing the original curves

``` r
suppressMessages(library(rms, quietly = TRUE))

fit <- lrm(y ~ pol(x,2), data = exposure_response)

xYplot(Cbind(yhat, lower, upper) ~ x
     , data = Predict(fit, x = 10*(0:40), fun = plogis, conf.int = 0.9)
     , type = 'l', method = 'filled bands', col.fill = 'gray'
     , ylim = 0:1
       )
```

<figure>
<img src="FZ_files/figure-commonmark/fig-repro-1.png" id="fig-repro"
alt="Figure 2: Attempt at straightforward reproduction of the figure, which was originally generated from “a binary logistic regression with quadratic function”." />
<figcaption aria-hidden="true">Figure 2: Attempt at straightforward
reproduction of the figure, which was originally generated from “a
binary logistic regression with quadratic function”.</figcaption>
</figure>

### Estimating a less restricted model

``` r
fit2 <- lrm(y ~ rcs(x,4), data = exposure_response)

xYplot(Cbind(yhat, lower, upper) ~ x
     , data = Predict(fit2, x = 10*(0:40), fun = plogis, conf.int = 0.9)
     , type = 'l', method = 'filled bands', col.fill = 'gray'
     , ylim = 0:1
       )
```

<figure>
<img src="FZ_files/figure-commonmark/fig-redux-rcs-1.png"
id="fig-redux-rcs"
alt="Figure 3: Figure 2, revisited using a restricted cubic spline with 4 knots. The “bell-shaped” appearance of Fig 1B and Figure 2 thus appears attributable to the analysts’ arbitrary choice of a quadratic fitting function, not to the actual content of the data." />
<figcaption aria-hidden="true">Figure 3: <a
href="#fig-repro">Figure 2</a>, revisited using a restricted cubic
spline with 4 knots. The “bell-shaped” appearance of Fig 1B and <a
href="#fig-repro">Figure 2</a> thus appears attributable to the
analysts’ arbitrary choice of a quadratic fitting function, not to the
actual content of the data.</figcaption>
</figure>

## What purpose did Fig 1B serve?

That Fig 1B, despite its blatant technical flaws and unnecessarily
tendentious content, features so prominently in Fourie Zirkelbach et al.
(2022), tells us that it serves an important purpose. The gap between
the relatively cautious discussion of Figure 15 in CDER (2018), and the
freewheeling discourse of Fourie Zirkelbach et al. (2022) around Fig 1B,
helps to reveal this.

(CDER 2018, p193) seems quite forthright in cautioning that

> The reason for low response rate in patients with higher steady state
> exposure was not identified. The relationships of ORR vs. serum
> phosphate concentration change from baseline and ORR vs. pemigatinib
> exposure are inconclusive.

Yet Fourie Zirkelbach et al. (2022) minimize uncertainty with an
extraordinary amount of weasel-wording:

<img src="weasel-wording.png"
data-fig-alt="In another example, the model-predicted relationship between overall response rate (ORR) and pemigatinib concentration (exposure) and serum phosphate concentration change from baseline follow apparent bell-shaped curves over the range of doses tested clinically (Fig 1B). Despite there being some uncertainty in the exact shape of these exposure response curves for efficacy, which may be associated with imbalances with respect to patient factors, large overlapping variability at the higher exposure quartiles, and potentially higher rates of dose interruptions and dose reductions at higher exposure quartiles,22 the final dosage for approval was supported by these relationships. Specifically, the modelpredicted proportion of patients with objective tumor responses at higher doses or exposures reached a peak while higher exposures than the approved dosage were also associated with increased exposure-related AEs or decreased tolerability, as is the case for pemigatinib." />

Among the several aspects of [weasel
words](https://en.wikipedia.org/wiki/Weasel_word) that make them
rhetorically useful, it is the *anonymous authority* they generate here
which I think best hints at the purpose of Fig 1B. On behalf of a
tendentious and impressionistic curve-fitting exercise, the authors of
this passage conjure up the full authority of a ‘prediction model’ such
as would presumably be derived from a pharmacologically-informed
modeling effort that aimed to gauge the “exact shape” of
exposure-response, and that has judiciously weighed “some” \[modest\]
amount of residual uncertainty. Similarly, Fig 1B acts as a kind of
weasel-graphic meant to authorize the single ‘optimum’ of ‘dose
optimization’, detaching this concept from considerations of
inter-individual heterogeneity in the balance of therapeutic and adverse
effects. The extraordinarily muddy treatment these considerations
receive in the quotation above reveals an intentional disregard for
them.

## Connections with Project Optimus

This manner of argumentation and sensibility about ‘underlying
statistical principles’ has apparently passed muster with the *Journal
of Clinical Oncology*; what if it became generally accepted? We might
then find ourselves in a statistical Wild West where anything goes,
where randomized dose assignment is employed to justify “sufficient
assessment of activity, safety and tolerability for each dosage” but
“does not need to be powered to demonstrate statistical superiority … or
non-inferiority” (OCE 2023, p5). This, in short, is the programme of
[Project
Optimus](https://www.fda.gov/about-fda/oncology-center-excellence/project-optimus).
Helpful background on the thinking behind this effort can be found in
the webinar transcript linked [in this Datamethods
discussion](https://discourse.datamethods.org/t/anomal-pharm-randomized-good-statistical-bad/5086?u=davidcnorrismd).

## References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-fda_cder_ndabla_2018" class="csl-entry">

CDER, FDA. 2018. “NDA/BLA Multi-Disciplinary Review and Evaluation NDA
213736 PEMAZYRE (Pemigatinib).” Reference ID: 4594191. US FDA Center for
Drug Evaluation & Research (CDER).
<https://www.accessdata.fda.gov/drugsatfda_docs/nda/2020/213736Orig1s000TOC.cfm>.

</div>

<div id="ref-fourie_zirkelbach_improving_2022" class="csl-entry">

Fourie Zirkelbach, Jeanne, Mirat Shah, Jonathon Vallejo, Joyce Cheng,
Amal Ayyoub, Jiang Liu, Rachel Hudson, et al. 2022. “Improving
Dose-Optimization Processes Used in Oncology Drug Development to
Minimize Toxicity and Maximize Benefit to Patients.” *Journal of
Clinical Oncology*, September, JCO.22.00371.
<https://doi.org/10.1200/JCO.22.00371>.

</div>

<div id="ref-gelman_bayesian_2014" class="csl-entry">

Gelman, Andrew, John B. Carlin, Hal S. Stern, David B. Dunson, Aki
Vehtari, and Donald B. Rubin. 2014. *Bayesian Data Analysis*. Third
edition. Chapman & Hall/CRC Texts in Statistical Science. Boca Raton:
CRC Press.

</div>

<div id="ref-oce_optimizing_2023" class="csl-entry">

OCE, FDA. 2023. “Optimizing the Dosage of Human Prescription Drugs and
Biological Products for the Treatment of Oncologic Diseases.” *U.S. Food
and Drug Administration*.
<https://www.fda.gov/regulatory-information/search-fda-guidance-documents/optimizing-dosage-human-prescription-drugs-and-biological-products-treatment-oncologic-diseases>.

</div>

</div>
