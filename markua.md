Exposing some ideological subtext in Fourie Zirkelbach et al (2022)
================
David C. Norris
2/20/23

{class: r, class: cell-code}
```
library(data.table)
library(jpeg)

## The ... argument lets us pass e.g. 'axes = TRUE' for the initial registration plot,
## and then 'axes = FALSE' upon subsequent use, when they are superfluous and take up
## too much space.
underlying <- function(image, xlim, ylim, ...) {
  
  plot(0:1, xlim = xlim, ylim = ylim
     , type='n', main=""
     , ...)
  
  rasterImage(png::readPNG(image)
             ,xleft = xlim[1]
             ,ybottom = ylim[1]
             ,xright = xlim[2]
             ,ytop = ylim[2]
              )
}
```

{id: introduction, number: 1}
## Introduction

Although Fourie Zirkelbach et al. (2022) promise to "discuss the
underlying statistical principles" of "strategies to integrate dose
optimization into premarketing drug development", this paper contains no
overt discussion, nor even a clear articulation, of any statistical
principle. Rather, it exhibits statistical
[anti-patterns](https://en.wikipedia.org/wiki/Anti-pattern) that
prefigure the 'dose optimization' methods advocated by FDA’s Oncology
Center of Excellence in recently issued Draft Guidance (OCE 2023).

{id: fig-1b, number: 2}
## Fig 1B

Below I reproduce the article’s Fig 1B, with an overlay registering my
best-effort digitization.

{class: r, class: cell-code}
```
## Read in digitized Fig 1B data, and overlay it on
## a png image, to prove registration is correct.

## TODO: Check that original study tallied 25 responses and 39 non,
##       for a total of N=64 participants.

fig1B <- function(...)
  underlying(image = "FZ-Fig1B.png"
           , xlim = c(-95, 538)
           , ylim = c(-.647, 1.304)
           , ...)

fig1B(axes = TRUE, xlab = "x", ylab = "y")
overlay <- adjustcolor("black", alpha.f = 0.35)
abline(v = c(0, 500), lty = 2, col= overlay)
abline(h = c(0, 1), lty = 2, col = overlay)

load("exposure_response.rda")
with(exposure_response, points(x, y, pch="+", col=overlay))

load("meancurve.rda")
with(meancurve, points(x, y, pch=20, col=overlay))

load("uclcurve.rda")
with(uclcurve, points(x, y, pch=20, col=overlay))

load("lclcurve.rda")
with(lclcurve, points(x, y, pch=20, col=overlay))
```

{id: fig-1B, alt: "Figure 1: Registration of digitized data from Fourie Zirkelbach et al.
(2022), Fig 1B. The original caption reads, "Examples of
exposure-efficacy relationships. … (B) Model-predicted versus observed
relationship of pemigatinib exposure (Cmax, ss) and ORR. Red squares
represent first (49.3–141 nM), second (143–185 nM), third (186–241 nM),
and fourth (248–492 nM) quartiles of pemigatinib Cmax at steady state
(Cmax, ss). Based on applicant figure reproduced in FDA
Multidisciplinary review of pemigatinib." Letting a probability
confidence bound dip once into negative territory may be regarded as a
misfortune, but allowing it to happen twice looks like carelessness."}
![](FZ_files/figure-markua/fig-1B-1.png)

{id: fitted-beta-distributions, number: 3}
## Fitted Beta distributions

Where statistical principles are taken seriously, expressions of
uncertainty mean something. Attempting to take at least the predicted
mean and *upper* confidence bound in Fig 1B seriously, what do they
imply?

One way to evaluate the implied informational content of the confidence
band in Fig 1B is by fitting Beta distributions to the estimated
response probabilities. We use the method of moments (Gelman et al.
2014, Eq (A.3), p583).

{class: r, class: cell-code}
```
xout <- seq(0, 400, 25)

df <- data.frame(x = xout
               , μ = with(meancurve, spline(x, y, xout=xout)$y)
               , ucl = with(uclcurve, spline(x, y, xout=xout)$y)
                 )

## Add method-of-moments estimates; see BDA3 Eq (A.3), p. 583.
df <- Hmisc::upData(df
                  , stderr = abs(ucl - μ)/qnorm(0.95)
                  , `α+β` = μ*(1-μ)/stderr^2 - 1
                  , α = `α+β`*μ
                  , β = `α+β` - α
                  , print = FALSE
                    )

knitr::kable(df, digits = 4)
```

|   x |      μ |    ucl | stderr |     α+β |       α |       β |
|----:|-------:|-------:|-------:|--------:|--------:|--------:|
|   0 | 0.0088 | 0.0336 | 0.0151 | 37.4802 |  0.3308 | 37.1494 |
|  25 | 0.0204 | 0.0780 | 0.0350 | 15.2738 |  0.3115 | 14.9623 |
|  50 | 0.0493 | 0.1651 | 0.0704 |  8.4431 |  0.4160 |  8.0270 |
|  75 | 0.1224 | 0.2772 | 0.0941 | 11.1325 |  1.3630 |  9.7696 |
| 100 | 0.2099 | 0.3714 | 0.0982 | 16.2103 |  3.4027 | 12.8076 |
| 125 | 0.3052 | 0.4507 | 0.0885 | 26.0998 |  7.9654 | 18.1344 |
| 150 | 0.4066 | 0.5199 | 0.0689 | 49.8290 | 20.2603 | 29.5687 |
| 175 | 0.4588 | 0.5865 | 0.0776 | 40.2055 | 18.4459 | 21.7596 |
| 200 | 0.4761 | 0.6190 | 0.0869 | 32.0648 | 15.2671 | 16.7978 |
| 225 | 0.4543 | 0.5939 | 0.0848 | 33.4422 | 15.1935 | 18.2487 |
| 250 | 0.3868 | 0.5391 | 0.0926 | 26.6580 | 10.3107 | 16.3473 |
| 275 | 0.2939 | 0.4727 | 0.1087 | 16.5680 |  4.8694 | 11.6986 |
| 300 | 0.1974 | 0.3887 | 0.1163 | 10.7113 |  2.1140 |  8.5973 |
| 325 | 0.1097 | 0.2703 | 0.0976 |  9.2513 |  1.0152 |  8.2361 |
| 350 | 0.0488 | 0.1622 | 0.0690 |  8.7612 |  0.4276 |  8.3336 |
| 375 | 0.0189 | 0.0789 | 0.0365 | 12.9046 |  0.2435 | 12.6611 |
| 400 | 0.0091 | 0.0327 | 0.0143 | 42.8959 |  0.3905 | 42.5054 |

On the view that the `\mathrm{Beta}(\alpha, \beta)`$ distribution is
equivalent to having observed `\alpha-1`$ successes and `\beta-1`$
failures in `\alpha+\beta-2`$ Bernoulli trials (Gelman et al. 2014,
p35), the large implied `\beta`$’s and `\alpha < 1`$ for `x > 350`$
suggest that the original Fig 1B embodies strong priors far beyond the
actual data collected — and indeed *beyond any data that could possibly
be collected,* since `\alpha < 1`$ implies observation of fewer than
zero sucesses. Thus it becomes clear that, beyond to the actual trial
data collected, Fig 1B incorporates a large contribution of
statistically uncontrolled impressionistic content.

{id: on-the-model-used-for-prediction, number: 4}
## On the 'model' used for 'prediction'

The origins of Fig 1B are to be found in a figure from Incyte’s NDA
submission, reproduced as Figure 15 in (CDER 2018, p192):

{fig-alt: Figure 15 from an FDA Multidisciplinary Review and Evaluation of Incyte's NDA submission.  The accompanying text reads, 'The exposure-response relationship of ORR and pemigatinib exposure was also evaluated with participants in study INCB 54828-202 Cohort A. The relationship followed a bell-shaped curve and a binary logistic regression with quadratic function was used to evaluate the relationship. Cmax,ss was identified to have the strongest association with ORR based on AIC criteria. The model predicted ORR (42.8%) at dose 13.5 mg is similar as the ORR (44%) predicted from change in serum phosphate concentration from baseline. Model predicted vs. observed relationship of ORR and Cmax,ss was shown in Figure 15.'}
![](MDRE-Fig15-plustext.png)

Thus, the "model-predicted ORR" proves to be anything but. Its 'model'
is nothing more than an arbitrarily selected functional form lacking
substantive pharmacologic rationale. Nor is there anything 'pre' about
this decidedly *post*dictive curve-fitting exercise. We can throw this
arbitrariness into sharper relief by attempting our own reproductions of
the figure.

{id: reproducing-the-original-curves, number: 4.1}
### Reproducing the original curves

{class: r, class: cell-code}
```
suppressMessages(library(rms, quietly = TRUE))

fit <- lrm(y ~ pol(x,2), data = exposure_response)

xYplot(Cbind(yhat, lower, upper) ~ x
     , data = Predict(fit, x = 10*(0:40), fun = plogis, conf.int = 0.9)
     , type = 'l', method = 'filled bands', col.fill = 'gray'
     , ylim = 0:1
       )
```

{id: fig-repro, alt: "Figure 2: Attempt at straightforward reproduction of the figure, which
was originally generated from "a binary logistic regression with
quadratic function"."}
![](FZ_files/figure-markua/fig-repro-1.png)

{id: estimating-a-less-restricted-model, number: 4.2}
### Estimating a less restricted model

{class: r, class: cell-code}
```
fit2 <- lrm(y ~ rcs(x,4), data = exposure_response)

xYplot(Cbind(yhat, lower, upper) ~ x
     , data = Predict(fit2, x = 10*(0:40), fun = plogis, conf.int = 0.9)
     , type = 'l', method = 'filled bands', col.fill = 'gray'
     , ylim = 0:1
       )
```

{id: fig-redux-rcs, alt: "Figure 3: [Figure 2](#fig-repro), revisited using a restricted cubic
spline with 4 knots. The "bell-shaped" appearance of Fig 1B and
[Figure 2](#fig-repro) thus appears attributable to the analysts’
arbitrary choice of a quadratic fitting function, not to the actual
content of the data."}
![](FZ_files/figure-markua/fig-redux-rcs-1.png)

{id: what-purpose-did-fig-1b-serve, number: 5}
## What purpose did Fig 1B serve?

That Fig 1B, despite its blatant technical flaws and unnecessarily
tendentious content, features so prominently in Fourie Zirkelbach et al.
(2022), tells us that it serves an important purpose. The gap between
the relatively cautious discussion of Figure 15 in CDER (2018), and the
freewheeling discourse of Fourie Zirkelbach et al. (2022) around Fig 1B,
helps to reveal this.

(CDER 2018, p193) seems quite forthright in cautioning that

> The reason for low response rate in patients with higher steady state
> exposure was not identified. The relationships of ORR vs. serum
> phosphate concentration change from baseline and ORR vs. pemigatinib
> exposure are inconclusive.

Yet Fourie Zirkelbach et al. (2022) minimize uncertainty with an
extraordinary amount of weasel-wording:

{fig-alt: In another example, the model-predicted relationship between overall response rate (ORR) and pemigatinib concentration (exposure) and serum phosphate concentration change from baseline follow apparent bell-shaped curves over the range of doses tested clinically (Fig 1B). Despite there being some uncertainty in the exact shape of these exposure response curves for efficacy, which may be associated with imbalances with respect to patient factors, large overlapping variability at the higher exposure quartiles, and potentially higher rates of dose interruptions and dose reductions at higher exposure quartiles,22 the final dosage for approval was supported by these relationships. Specifically, the modelpredicted proportion of patients with objective tumor responses at higher doses or exposures reached a peak while higher exposures than the approved dosage were also associated with increased exposure-related AEs or decreased tolerability, as is the case for pemigatinib.}
![](weasel-wording.png)

Among the several aspects of [weasel
words](https://en.wikipedia.org/wiki/Weasel_word) that make them
rhetorically useful, it is the *anonymous authority* they generate here
which I think best hints at the purpose of Fig 1B. On behalf of a
tendentious and impressionistic curve-fitting exercise, the authors of
this passage conjure up the full authority of a 'prediction model' such
as would presumably be derived from a pharmacologically-informed
modeling effort that aimed to gauge the "exact shape" of
exposure-response, and that has judiciously weighed "some" [modest]
amount of residual uncertainty. Similarly, Fig 1B acts as a kind of
weasel-graphic meant to authorize the single 'optimum' of 'dose
optimization', detaching this concept from considerations of
inter-individual heterogeneity in the balance of therapeutic and adverse
effects. The extraordinarily muddy treatment these considerations
receive in the quotation above reveals an intentional disregard for
them.

{id: connections-with-project-optimus, number: 6}
## Connections with Project Optimus

This manner of argumentation and sensibility about 'underlying
statistical principles' has apparently passed muster with the *Journal
of Clinical Oncology*; what if it became generally accepted? We might
then find ourselves in a statistical Wild West where anything goes,
where randomized dose assignment is employed to justify "sufficient
assessment of activity, safety and tolerability for each dosage" but
"does not need to be powered to demonstrate statistical superiority … or
non-inferiority" (OCE 2023, p5). This, in short, is the programme of
[Project
Optimus](https://www.fda.gov/about-fda/oncology-center-excellence/project-optimus).
Helpful background on the thinking behind this effort can be found in
the webinar transcript linked [in this Datamethods
discussion](https://discourse.datamethods.org/t/anomal-pharm-randomized-good-statistical-bad/5086?u=davidcnorrismd).

{id: references, class: unnumbered, number: 7}
## References

CDER, FDA. 2018. `“`NDA`/`BLA` `Multi`-Disciplinary `Review` and
`Evaluation` `NDA` 213736 `PEMAZYRE` (Pemigatinib).”` Reference ID:
4594191. US FDA Center for Drug Evaluation & Research (CDER).
[https:~/~/www.accessdata.fda.gov/drugsatfda_docs/nda/2020/213736Orig1s000TOC.cfm](https://www.accessdata.fda.gov/drugsatfda_docs/nda/2020/213736Orig1s000TOC.cfm).

Fourie Zirkelbach, Jeanne, Mirat Shah, Jonathon Vallejo, Joyce Cheng,
Amal Ayyoub, Jiang Liu, Rachel Hudson, et al. 2022. `“Improving
`Dose`-`Optimization` `Processes` `Used` in `Oncology` `Drug`
`Development` to `Minimize` `Toxicity` and `Maximize` `Benefit` to
`Patients`.”` *Journal of Clinical Oncology*, September, JCO.22.00371.
[https:~/~/doi.org/10.1200/JCO.22.00371](https://doi.org/10.1200/JCO.22.00371).

Gelman, Andrew, John B. Carlin, Hal S. Stern, David B. Dunson, Aki
Vehtari, and Donald B. Rubin. 2014. *Bayesian Data Analysis*. Third
edition. Chapman & `Hall`/`CRC` Texts in Statistical Science. Boca
Raton: CRC Press.

OCE, FDA. 2023. `“Optimizing the `Dosage` of `Human` `Prescription`
`Drugs` and `Biological` `Products` for the `Treatment` of `Oncologic`
`Diseases`.”` *U.S. Food and Drug Administration*.
[https:~/~/www.fda.gov/regulatory-information/search-fda-guidance-documents/optimizing-dosage-human-prescription-drugs-and-biological-products-treatment-oncologic-diseases](https://www.fda.gov/regulatory-information/search-fda-guidance-documents/optimizing-dosage-human-prescription-drugs-and-biological-products-treatment-oncologic-diseases).
